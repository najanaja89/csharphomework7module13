﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework7Module13
{
    public class Methods
    {
        public void SecondMax<T>(List<T> list)
        {
            list.Sort();
            Console.WriteLine("Second max element is " + list[(list.Count()) - 2]);

        }

        public void EvenSum(List<int> list)
        {
            int sum = 0;
            for (int i = 0; i < list.Count; i++)
            {
                if (i % 2 == 0)
                {
                    sum += list[i];
                }
            }
            Console.WriteLine("Sum of even elements is " + sum);
        }

    }
}
