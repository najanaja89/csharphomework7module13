﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharpHomework7Module13
{
    class Program
    {

        static void Main(string[] args)
        {
            var randr = new Random();
            var methods = new Methods();

            var list = new List<int>()
            {
                randr.Next(1, 100),
                randr.Next(1, 100),
                randr.Next(1, 100),
                randr.Next(1, 100),
                randr.Next(1, 100),
                randr.Next(1, 100),
                randr.Next(1, 100),
                randr.Next(1, 100),
                randr.Next(1, 100)
            };

            list.Sort();
            Console.WriteLine();
            foreach (var item in list)
            {
                Console.Write(item + "\t");
            }
            Console.WriteLine();
            Console.WriteLine();

            methods.SecondMax(list);
            methods.EvenSum(list);
            Console.WriteLine();

            Console.ReadLine();


        }


    }
}
